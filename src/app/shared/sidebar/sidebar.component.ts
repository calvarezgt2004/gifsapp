import { Component, OnInit } from '@angular/core';
import { GifsService } from 'src/app/gifs/service/gifs.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {

  constructor(private giftService: GifsService) { }

  ngOnInit(): void {
  }

  get historial(){
    return this.giftService.historial;
  }  

  buscar(termino: string){
    this.giftService.buscarGifs(termino);
  }

}
